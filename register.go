package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)


const (
	dbConnStr = "user=postgres password=admin dbname=Q07_DB host=localhost port=5432 sslmode=disable"
)


type User struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

func main() {
	
	db, err := sql.Open("postgres", dbConnStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	
	router := gin.Default()

	
	router.POST("/register", func(c *gin.Context) {
		var user User
		if err := c.BindJSON(&user); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		
		_, err := db.Exec("INSERT INTO Users (UserName, Password) VALUES ($1, $2)", user.UserName, user.Password)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "User registered successfully"})
	})

	
	router.Run(":8080")
}
