package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

type Notification struct {
	NotificationID int    `json:"notification_id"`
	Content        string `json:"content"`
	SentDate       string `json:"sent_date"`
}

var db *sql.DB

func main() {
	var err error
	db, err = sql.Open("postgres", "user=postgres password=admin dbname=Q07_DB host=localhost port=5432 sslmode=disable")
	if err != nil {
		log.Fatal("Could not connect to database:", err)
	}
	defer db.Close()

	r := gin.Default()

	r.GET("/notifications/:username", getUserNotifications)

	r.Run(":8080")
}

func getUserNotifications(c *gin.Context) {

	username := c.Param("username")

	rows, err := db.Query(`
        SELECT n.NotificationID, n.Content, n.SentDate
        FROM Notifications n
        JOIN Notification_User nu ON n.NotificationID = nu.NotificationID
        JOIN Users u ON nu.UserID = u.UserID
        WHERE u.UserName = $1
    `, username)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	notifications := make([]Notification, 0)
	for rows.Next() {
		var notification Notification
		err := rows.Scan(&notification.NotificationID, &notification.Content, &notification.SentDate)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		notifications = append(notifications, notification)
	}

	c.JSON(http.StatusOK, notifications)
}
