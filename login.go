package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)


const (
	dbConnStr = "user=postgres password=admin dbname=Q07_DB host=localhost port=5432 sslmode=disable"
)

// User struct
type User struct {
	UserID   int    `json:"user_id"`
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

func main() {
	
	db, err := sql.Open("postgres", dbConnStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	
	router := gin.Default()

	
	router.POST("/login", func(c *gin.Context) {
		var user User
		if err := c.BindJSON(&user); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var dbPassword string
		err := db.QueryRow("SELECT Password FROM Users WHERE UserName = $1", user.UserName).Scan(&dbPassword)
		switch {
		case err == sql.ErrNoRows:
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid username or password"})
			return
		case err != nil:
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Database error"})
			return
		}

		if dbPassword != user.Password {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid username or password"})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Login successful"})
	})

	
	router.Run(":8080")
}
