PGDMP      ;    	            |            Q07_DB    16.2    16.2 6    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16752    Q07_DB    DATABASE     �   CREATE DATABASE "Q07_DB" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE "Q07_DB";
                postgres    false            �            1259    16753    notification_role    TABLE     l   CREATE TABLE public.notification_role (
    notificationid integer NOT NULL,
    roleid integer NOT NULL
);
 %   DROP TABLE public.notification_role;
       public         heap    postgres    false            �            1259    16756 $   notification_role_notificationid_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_role_notificationid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.notification_role_notificationid_seq;
       public          postgres    false    215            �           0    0 $   notification_role_notificationid_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.notification_role_notificationid_seq OWNED BY public.notification_role.notificationid;
          public          postgres    false    216            �            1259    16757    notification_user    TABLE     l   CREATE TABLE public.notification_user (
    notificationid integer NOT NULL,
    userid integer NOT NULL
);
 %   DROP TABLE public.notification_user;
       public         heap    postgres    false            �            1259    16760 $   notification_user_notificationid_seq    SEQUENCE     �   CREATE SEQUENCE public.notification_user_notificationid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.notification_user_notificationid_seq;
       public          postgres    false    217            �           0    0 $   notification_user_notificationid_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.notification_user_notificationid_seq OWNED BY public.notification_user.notificationid;
          public          postgres    false    218            �            1259    16761    notifications    TABLE     �   CREATE TABLE public.notifications (
    notificationid integer NOT NULL,
    content text,
    sentdate timestamp without time zone
);
 !   DROP TABLE public.notifications;
       public         heap    postgres    false            �            1259    16766     notifications_notificationid_seq    SEQUENCE     �   CREATE SEQUENCE public.notifications_notificationid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.notifications_notificationid_seq;
       public          postgres    false    219            �           0    0     notifications_notificationid_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.notifications_notificationid_seq OWNED BY public.notifications.notificationid;
          public          postgres    false    220            �            1259    16767    roles    TABLE     _   CREATE TABLE public.roles (
    roleid integer NOT NULL,
    rolename character varying(50)
);
    DROP TABLE public.roles;
       public         heap    postgres    false            �            1259    16770    roles_roleid_seq    SEQUENCE     �   CREATE SEQUENCE public.roles_roleid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.roles_roleid_seq;
       public          postgres    false    221            �           0    0    roles_roleid_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.roles_roleid_seq OWNED BY public.roles.roleid;
          public          postgres    false    222            �            1259    16771 	   user_role    TABLE     \   CREATE TABLE public.user_role (
    userid integer NOT NULL,
    roleid integer NOT NULL
);
    DROP TABLE public.user_role;
       public         heap    postgres    false            �            1259    16774    user_role_roleid_seq    SEQUENCE     �   CREATE SEQUENCE public.user_role_roleid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.user_role_roleid_seq;
       public          postgres    false    223            �           0    0    user_role_roleid_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.user_role_roleid_seq OWNED BY public.user_role.roleid;
          public          postgres    false    224            �            1259    16775    user_role_userid_seq    SEQUENCE     �   CREATE SEQUENCE public.user_role_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.user_role_userid_seq;
       public          postgres    false    223            �           0    0    user_role_userid_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.user_role_userid_seq OWNED BY public.user_role.userid;
          public          postgres    false    225            �            1259    16776    users    TABLE     �   CREATE TABLE public.users (
    userid integer NOT NULL,
    username character varying(50),
    password character varying(50)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    16779    users_userid_seq    SEQUENCE     �   CREATE SEQUENCE public.users_userid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.users_userid_seq;
       public          postgres    false    226            �           0    0    users_userid_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.users_userid_seq OWNED BY public.users.userid;
          public          postgres    false    227            4           2604    16780     notification_role notificationid    DEFAULT     �   ALTER TABLE ONLY public.notification_role ALTER COLUMN notificationid SET DEFAULT nextval('public.notification_role_notificationid_seq'::regclass);
 O   ALTER TABLE public.notification_role ALTER COLUMN notificationid DROP DEFAULT;
       public          postgres    false    216    215            5           2604    16781     notification_user notificationid    DEFAULT     �   ALTER TABLE ONLY public.notification_user ALTER COLUMN notificationid SET DEFAULT nextval('public.notification_user_notificationid_seq'::regclass);
 O   ALTER TABLE public.notification_user ALTER COLUMN notificationid DROP DEFAULT;
       public          postgres    false    218    217            6           2604    16782    notifications notificationid    DEFAULT     �   ALTER TABLE ONLY public.notifications ALTER COLUMN notificationid SET DEFAULT nextval('public.notifications_notificationid_seq'::regclass);
 K   ALTER TABLE public.notifications ALTER COLUMN notificationid DROP DEFAULT;
       public          postgres    false    220    219            7           2604    16783    roles roleid    DEFAULT     l   ALTER TABLE ONLY public.roles ALTER COLUMN roleid SET DEFAULT nextval('public.roles_roleid_seq'::regclass);
 ;   ALTER TABLE public.roles ALTER COLUMN roleid DROP DEFAULT;
       public          postgres    false    222    221            8           2604    16784    user_role userid    DEFAULT     t   ALTER TABLE ONLY public.user_role ALTER COLUMN userid SET DEFAULT nextval('public.user_role_userid_seq'::regclass);
 ?   ALTER TABLE public.user_role ALTER COLUMN userid DROP DEFAULT;
       public          postgres    false    225    223            9           2604    16785    user_role roleid    DEFAULT     t   ALTER TABLE ONLY public.user_role ALTER COLUMN roleid SET DEFAULT nextval('public.user_role_roleid_seq'::regclass);
 ?   ALTER TABLE public.user_role ALTER COLUMN roleid DROP DEFAULT;
       public          postgres    false    224    223            :           2604    16786    users userid    DEFAULT     l   ALTER TABLE ONLY public.users ALTER COLUMN userid SET DEFAULT nextval('public.users_userid_seq'::regclass);
 ;   ALTER TABLE public.users ALTER COLUMN userid DROP DEFAULT;
       public          postgres    false    227    226            �          0    16753    notification_role 
   TABLE DATA           C   COPY public.notification_role (notificationid, roleid) FROM stdin;
    public          postgres    false    215   %=       �          0    16757    notification_user 
   TABLE DATA           C   COPY public.notification_user (notificationid, userid) FROM stdin;
    public          postgres    false    217   T=       �          0    16761    notifications 
   TABLE DATA           J   COPY public.notifications (notificationid, content, sentdate) FROM stdin;
    public          postgres    false    219   �=       �          0    16767    roles 
   TABLE DATA           1   COPY public.roles (roleid, rolename) FROM stdin;
    public          postgres    false    221   >       �          0    16771 	   user_role 
   TABLE DATA           3   COPY public.user_role (userid, roleid) FROM stdin;
    public          postgres    false    223   ^>       �          0    16776    users 
   TABLE DATA           ;   COPY public.users (userid, username, password) FROM stdin;
    public          postgres    false    226   {>       �           0    0 $   notification_role_notificationid_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.notification_role_notificationid_seq', 1, false);
          public          postgres    false    216            �           0    0 $   notification_user_notificationid_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.notification_user_notificationid_seq', 1, false);
          public          postgres    false    218            �           0    0     notifications_notificationid_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.notifications_notificationid_seq', 5, true);
          public          postgres    false    220            �           0    0    roles_roleid_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.roles_roleid_seq', 4, true);
          public          postgres    false    222            �           0    0    user_role_roleid_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.user_role_roleid_seq', 1, false);
          public          postgres    false    224            �           0    0    user_role_userid_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.user_role_userid_seq', 1, false);
          public          postgres    false    225            �           0    0    users_userid_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.users_userid_seq', 22, true);
          public          postgres    false    227            <           2606    16788 (   notification_role notification_role_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.notification_role
    ADD CONSTRAINT notification_role_pkey PRIMARY KEY (notificationid);
 R   ALTER TABLE ONLY public.notification_role DROP CONSTRAINT notification_role_pkey;
       public            postgres    false    215            >           2606    16790 (   notification_user notification_user_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY public.notification_user
    ADD CONSTRAINT notification_user_pkey PRIMARY KEY (notificationid);
 R   ALTER TABLE ONLY public.notification_user DROP CONSTRAINT notification_user_pkey;
       public            postgres    false    217            @           2606    16792     notifications notifications_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (notificationid);
 J   ALTER TABLE ONLY public.notifications DROP CONSTRAINT notifications_pkey;
       public            postgres    false    219            B           2606    16794    roles roles_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (roleid);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public            postgres    false    221            D           2606    16796    user_role user_role_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (userid, roleid);
 B   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_pkey;
       public            postgres    false    223    223            F           2606    16798    users users_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    226            G           2606    16799 /   notification_role notification_role_roleid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification_role
    ADD CONSTRAINT notification_role_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(roleid);
 Y   ALTER TABLE ONLY public.notification_role DROP CONSTRAINT notification_role_roleid_fkey;
       public          postgres    false    221    4674    215            H           2606    16804 /   notification_user notification_user_userid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.notification_user
    ADD CONSTRAINT notification_user_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);
 Y   ALTER TABLE ONLY public.notification_user DROP CONSTRAINT notification_user_userid_fkey;
       public          postgres    false    226    4678    217            I           2606    16809    user_role user_role_roleid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(roleid);
 I   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_roleid_fkey;
       public          postgres    false    221    4674    223            J           2606    16814    user_role user_role_userid_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);
 I   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_userid_fkey;
       public          postgres    false    226    223    4678            �      x�3�4�2�4�2�4�2�4�2�c���� '�      �   !   x�3�4�2�4�2�4�2�4�2�4����� '�      �   �   x�}�1�0�����@��lJ�R ��� �'��*:K[��j��2���?�-kxw��6�mH�փ�ӷ�Þ�i��R�-xW��Z�����5lat9o���Ŗ$�k|g�e���$�1����J���>z      �   0   x�3�tL����2���OI-J,�/�2�-N-�2�t/M-.����� ��
�      �      x������ � �      �   �   x�e�K
�0Dמ��d;�]�	44�6	�����Ѝ��i$�s7�u��ײ�)D*�_&�eb!Z�"*	�
��Ddwl�2�����cD�i�'��YESl�@&M�އ&d����)L��<%�J���{˷s(l{��M�!�&��)� 4�f�     